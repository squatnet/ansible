How to use our ansible playbook
===============================

Requirements
------------

* You must have `torsocks` and `nc` installed as the playbook makes sure connection
  is done through the Tor network.
* In ~/.ssh/config, you may want to have some lines like this:

    Host spica.tachanka.org
	User root
	IdentityFile /path/to/your/key
	ProxyCommand torsocks -i nc %h %p


Test your recipe
----------------

* `ansible-lint *.yml`
* Fix errors thrown by ansible-lint
* Dry-run the playbook: use --check (or -C) 


To run it
---------

`ansible-playbook -i squat.net-hosts.yml site.yml`

Usually ansible takes its list of hosts in /etc/ansible/hosts, but here we
tell it to take it from a file.

We can use "--steps" to go step-by-step with confirmation before a task runs.


Run a specific role or task
---------------------------

* Make sure the role or task you want to run has a tag.
  (as of today, roles have tags in the form of "myrole_role_tag")
* Run the playbook with --tags <tagname>


Split long lines
----------------

Ansible-lint will scream on lines longer than 160 chars.
See roles/backup_server/tasks/main.yml, the authorized_key module uses the
">-" yaml syntax to split a line.
