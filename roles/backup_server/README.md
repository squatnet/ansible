Borg server role
================

This role feeds the server that hosts our backups.

It configures it to receive backups from Radar, and maybe other comrade
servers (see `vars/main.yml`), using borgbackup in `--serve` mode (meaning
the clients will send their files) and `--append-only` (meaning the
clients won't be allowed to delete anything from this server).

TODO: cron'ed prune. Meanwhile, a free disk space script is run by cron 
everyday, and will send an email to `tech@` if more than 90% are used.

Requirements
------------

* Root access on the machine
* Borgbackup installed on the machine (it's done in the `common` role)
* UFW installed on the machine (done in `firewall.yml`)

Role Variables
--------------

See `vars/main.yml`, where we define hosts that are allowed to back things
up on this server.

Dependencies
------------

Depends on `common`.
