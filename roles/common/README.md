Common role
===========

This role is used by all servers managed by Ansible.

* Installs a bunch of packages
* Disables IPv6
* Setup for disk usage alerts

Requirements
------------

You need root access to play this role.

Role Variables
--------------

See `vars/main.yml`:

* `disk_usage_alert`: disk usage threshold

Dependencies
------------

No dependencies.
