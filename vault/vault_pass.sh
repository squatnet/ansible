#!/bin/sh

# Read s!n doc to find out how to add keys able to decrypt the vault

VAULT_PW_FILENAME="vault/vault-passphrase"
gpg --quiet --batch --use-agent --decrypt ${VAULT_PW_FILENAME}
